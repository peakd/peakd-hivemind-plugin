import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/sequelize';
import { Sequelize } from 'sequelize-typescript';
import { Suggestion } from './models/suggestion.model';

// retrieve the badge assigned to an account (following the account and not ignored)
const SUGGESTIONS_BY_ACCOUNT = `
WITH account as (
  select id
  from hive_accounts a
  where "name" = :account
)
select suggested."name" as suggestion
     , main.score
     , (log(main.score * 100) + (0.5 * main.score)) * log(2000000 / suggested.followers) "rank"
from (
  select a.id
     , suggestion."following"
     , count(*)::decimal / max(count(*)) OVER (PARTITION BY a.id) as score
  from account a
  join hive_follows base on base.follower = a.id and base.state = 1
  join hive_follows suggestion on suggestion.follower = base."following" and suggestion.state = 1
  where a.id != suggestion."following"
  group by a.id, suggestion."following"
) main
left join hive_follows excluded on excluded.follower = main.id and excluded."following" = main."following" and excluded.state > 0
join hive_accounts suggested on suggested.id = main."following"
where excluded."following" is null
and suggested.lastread_at > now() - interval '6 months'
and suggested.name not like 'hive-%'
and suggested.name not like 'badge-%'
order by "rank" desc
limit :limit
offset :offset
`;

const SUGGESTIONS_PAGE_SIZE = 50;

@Injectable()
export class AccountsService {
  constructor(
    private sequelize: Sequelize,
    private config: ConfigService,
    @InjectModel(Suggestion) private suggestionModel: typeof Suggestion,
  ) {}

  async suggestionsByAccount(
    account: string,
    page: number = 0,
  ): Promise<Suggestion[]> {
    if (this.config.get('database.schema')) {
      await this.sequelize.query(`SET search_path TO "${this.config.get('database.schema')}"`);
    }

    return await this.sequelize.query(SUGGESTIONS_BY_ACCOUNT, {
      replacements: {
        account: account,
        offset: page * SUGGESTIONS_PAGE_SIZE,
        limit: SUGGESTIONS_PAGE_SIZE,
      },
      model: Suggestion,
    });
  }
}
