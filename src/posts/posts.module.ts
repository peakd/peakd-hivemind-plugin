import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';
import { HivePost } from './models/hive-post.model';
import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';

@Module({
    imports: [ConfigService, SequelizeModule.forFeature([HivePost])],
    providers: [PostsService],
    controllers: [PostsController],
  })
export class PostsModule {}