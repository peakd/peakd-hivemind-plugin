import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/sequelize';
import { Sequelize } from 'sequelize-typescript';
import { HivePost } from './models/hive-post.model';

const POST_SELECT = `SELECT coalesce(v.votes, '[]'::json) as active_votes
  , p.author
  , CASE p.is_paidout
      WHEN true THEN (p.payout::decimal - split_part(p.curator_payout_value, ' ', 1)::decimal) || ' HBD'
      ELSE (p.pending_payout::decimal - split_part(p.curator_payout_value, ' ', 1)::decimal) || ' HBD'
    END as author_payout_value
  , p.author_rep as author_reputation
  , CASE coalesce(p.role_id, 0)
      WHEN -2 THEN 'muted'
      WHEN  2 THEN 'member'
      WHEN  4 THEN 'mod'
      WHEN  6 THEN 'admin'
      WHEN  8 THEN 'owner'
      ELSE 'guest'
    END as author_role
  , p.role_title as author_title
  , p.beneficiaries
  , null as blacklists
  , p.body
  , p.category
  , p.children
  , p.community_name as community
  , p.community_title
  , p.created_at as created
  , p.curator_payout_value
  , p."depth"
  , p.is_paidout
  , p."json"::json as json_metadata
  , p.max_accepted_payout
  , p.rshares::int8 as net_rshares
  , p.payout::numeric
  , p.payout_at
  , p.pending_payout || ' HBD' as pending_payout_value
  , p.percent_hbd
  , p.permlink
  , p.id as post_id
  , p.promoted || ' HBD' as promoted
  , '[]'::json as replies
  , json_build_object('total_votes', p.total_votes, 'gray', p.is_muted, 'is_pinned', p.is_pinned) as stats
  , p.title
  , p.updated_at as updated
  , p.url`;

const FEED_BY_AUTHORS = `
WITH authors as (
  select a.id
  from hive_accounts a
  where a.name in (:authors)
),
posts as (
  select p.id
  from authors a
  join live_posts_view p on p.author_id = a.id
  where p.created_at > now() - interval ':days days'
    and (:start = 0 or p.id < :start)
  order by p.created_at desc
  limit :limit
)
${POST_SELECT}
FROM posts i,
LATERAL get_post_view_by_id(i.id) p,
LATERAL(
  select json_agg(json_build_object('voter', a."name", 'rshares', v.rshares)) as votes
  from hive_votes v
  join hive_accounts a on a.id = v.voter_id
  where v.post_id = i.id) as v
order by p.created_at desc`;

const POSTS_BY_PERMLINKS = `
${POST_SELECT}
FROM hive_posts_api_helper i,
LATERAL get_post_view_by_id(i.id) p,
LATERAL(
  select json_agg(json_build_object('voter', a."name", 'rshares', v.rshares)) as votes
  from hive_votes v
  join hive_accounts a on a.id = v.voter_id
  where v.post_id = i.id) as v
WHERE p.depth = 0
      and i.author_s_permlink in (:permlinks)`;

const POSTS_IN_CATEGORY_BY_AUTHOR = `
WITH posts as (
  select id
  from live_posts_view
  where is_muted = false
    and author_id = (select id from hive_accounts where "name" = :author)
    and (community_id is null and category_id = (select id from hive_category_data where category = :category)
        or
        (community_id is not null and (select id from hive_tag_data where tag = :category) = tags_ids[1]))
    and (:start = 0 or id < :start)
  order by created_at desc
  limit :limit
)
${POST_SELECT}
FROM posts i,
LATERAL get_post_view_by_id(i.id) p,
LATERAL(
  select json_agg(json_build_object('voter', a."name", 'rshares', v.rshares)) as votes
  from hive_votes v
  join hive_accounts a on a.id = v.voter_id
  where v.post_id = i.id) as v
order by p.created_at desc`;

const POSTS_IN_COMMUNITY_BY_AUTHOR = `
WITH posts as (
  select id
  from live_posts_view
  where is_muted = false
    and author_id = (select id from hive_accounts where "name" = :author)
    and community_id = (select id from hive_communities where "name" = :community)
    and (:start = 0 or id < :start)
  order by created_at desc
  limit :limit
)
${POST_SELECT}
FROM posts i,
LATERAL get_post_view_by_id(i.id) p,
LATERAL(
  select json_agg(json_build_object('voter', a."name", 'rshares', v.rshares)) as votes
  from hive_votes v
  join hive_accounts a on a.id = v.voter_id
  where v.post_id = i.id) as v
order by p.created_at desc`;

const POSTS_IN_COMMUNITY_BY_TAG = `
WITH posts as (
  select id
  from live_posts_view
  where is_muted = false
    and community_id = (select id from hive_communities where "name" = :community)
    and (select id from hive_tag_data where tag = :tag) = ANY (tags_ids)
    and (:start = 0 or id < :start)
  order by created_at desc
  limit :limit
)
${POST_SELECT}
FROM posts i,
LATERAL get_post_view_by_id(i.id) p,
LATERAL(
  select json_agg(json_build_object('voter', a."name", 'rshares', v.rshares)) as votes
  from hive_votes v
  join hive_accounts a on a.id = v.voter_id
  where v.post_id = i.id) as v
order by p.created_at desc`;

@Injectable()
export class PostsService {
  constructor(
    private sequelize: Sequelize,
    private config: ConfigService,
    @InjectModel(HivePost) private postModel: typeof HivePost,
  ) {}

  async feedByAuthors(
    authors: string[],
    start: number = 0,
    limit: number = 20,
    daysInterval: number = 45,
  ): Promise<HivePost[]> {
    if (this.config.get('database.schema')) {
      await this.sequelize.query(`SET search_path TO "${this.config.get('database.schema')}"`);
    }

    return await this.sequelize.query(FEED_BY_AUTHORS, {
      replacements: {
        authors: authors,
        start: start,
        limit: limit,
        days: daysInterval,
      },
      model: HivePost,
    });
  }

  async postsByPermlinks(permlinks: string[]): Promise<HivePost[]> {
    if (!permlinks || permlinks.length === 0) {
      return [];
    }

    if (this.config.get('database.schema')) {
      await this.sequelize.query(`SET search_path TO "${this.config.get('database.schema')}"`);
    }

    return await this.sequelize.query(POSTS_BY_PERMLINKS, {
      replacements: {
        permlinks: permlinks,
      },
      model: HivePost,
    });
  }

  async postsInCategoryByAuthor(
    category: string,
    author: string,
    start: number = 0,
    limit: number = 20,
  ): Promise<HivePost[]> {
    if (this.config.get('database.schema')) {
      await this.sequelize.query(`SET search_path TO "${this.config.get('database.schema')}"`);
    }

    return await this.sequelize.query(POSTS_IN_CATEGORY_BY_AUTHOR, {
      replacements: {
        category: category,
        author: author,
        start: start,
        limit: limit,
      },
      model: HivePost,
    });
  }

  async postsInCommunityByAuthor(
    community: string,
    author: string,
    start: number = 0,
    limit: number = 20,
  ): Promise<HivePost[]> {
    if (this.config.get('database.schema')) {
      await this.sequelize.query(`SET search_path TO "${this.config.get('database.schema')}"`);
    }

    return await this.sequelize.query(POSTS_IN_COMMUNITY_BY_AUTHOR, {
      replacements: {
        community: community,
        author: author,
        start: start,
        limit: limit,
      },
      model: HivePost,
    });
  }

  async postsInCommunityByTag(
    community: string,
    tag: string,
    start: number = 0,
    limit: number = 20,
  ): Promise<HivePost[]> {
    if (this.config.get('database.schema')) {
      await this.sequelize.query(`SET search_path TO "${this.config.get('database.schema')}"`);
    }

    return await this.sequelize.query(POSTS_IN_COMMUNITY_BY_TAG, {
      replacements: {
        community: community,
        tag: tag,
        start: start,
        limit: limit,
      },
      model: HivePost,
    });
  }
}
