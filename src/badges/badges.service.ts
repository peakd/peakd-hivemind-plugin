import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/sequelize';
import { Sequelize } from 'sequelize-typescript';
import { Badge } from './models/badge.model';

// retrieve the badge assigned to an account (following the account and not ignored)
const BADGES_BY_RECIPIENT = `select badge.name
  , badge.created_at
  , badge.reputation
  , badge.followers
  , badge."following"
  , (coalesce(nullif(trim(badge.posting_json_metadata), ''), nullif(trim(badge.json_metadata), '')))::json->'profile'->'name' as title
from hive_follows f
join hive_accounts badge on badge.id = f.follower
join hive_accounts account on account.id = f."following"
left join hive_follows i on i.follower = account.id and i."following" = badge.id and i.state = 2
where badge."name" like 'badge-%'
  and f.state = 1
  and i.follower is null
  and account."name" = :account`;

// retrieve an account badge subscriptions (badges followed by an account)
const SUBSCRIPTIONS_BY_ACCOUNT = `select badge.name
  , badge.created_at
  , badge.reputation
  , badge.followers
  , badge."following"
  , (coalesce(nullif(trim(badge.posting_json_metadata), ''), nullif(trim(badge.json_metadata), '')))::json->'profile'->'name' as title
from hive_follows f
join hive_accounts account on account.id = f.follower
join hive_accounts badge on badge.id = f."following"
where badge."name" like 'badge-%'
  and f.state = 1
  and account."name" = :account`;

// find badges matching a query
const BADGES_BY_QUERY = `select badge.name
, badge.created_at
, badge.reputation
, badge.followers
, badge."following"
, (coalesce(nullif(trim(badge.posting_json_metadata), ''), nullif(trim(badge.json_metadata), '')))::json->'profile'->'name' as title
from hive_accounts badge
where badge."name" like 'badge-%'
and badge.posting_json_metadata like '{%}'
and lower(badge.posting_json_metadata::json->'profile'->>'name') like lower(:query)
limit :limit`;

@Injectable()
export class BadgesService {
  constructor(
    private sequelize: Sequelize,
    private config: ConfigService,
    @InjectModel(Badge) private badgeModel: typeof Badge,
  ) {}

  async listByRecipient(account: string): Promise<Badge[]> {
    if (this.config.get('database.schema')) {
      await this.sequelize.query(`SET search_path TO "${this.config.get('database.schema')}"`);
    }

    return await this.sequelize.query(BADGES_BY_RECIPIENT, {
      replacements: {
        account: account,
      },
      model: Badge,
    });
  }

  async listSubscriptionsByAccount(account: string): Promise<Badge[]> {
    if (this.config.get('database.schema')) {
      await this.sequelize.query(`SET search_path TO "${this.config.get('database.schema')}"`);
    }

    return await this.sequelize.query(SUBSCRIPTIONS_BY_ACCOUNT, {
      replacements: {
        account: account,
      },
      model: Badge,
    });
  }

  async searchByName(query: string, limit: number = 20): Promise<Badge[]> {
    if (this.config.get('database.schema')) {
      await this.sequelize.query(`SET search_path TO "${this.config.get('database.schema')}"`);
    }

    return await this.sequelize.query(BADGES_BY_QUERY, {
      replacements: {
        query: `%${query}%`,
        limit: limit,
      },
      model: Badge,
    });
  }
}
