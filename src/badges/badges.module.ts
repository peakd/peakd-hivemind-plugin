import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';
import { Badge } from './models/badge.model';
import { BadgesController } from './badges.controller';
import { BadgesService } from './badges.service';

@Module({
  imports: [ConfigService, SequelizeModule.forFeature([Badge])],
  providers: [BadgesService],
  controllers: [BadgesController],
})
export class BadgesModule {}
