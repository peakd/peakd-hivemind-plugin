# PeakD Hivemind Plugin

A Node.js application to add additional API to Hive/Hivemind nodes.

![cover](./resources/cover.png)

# Usage

```
# Install dependencies
npm install

# Serve on localhost:3000
npm run start
```

# Configuration

Supported configuration using both environment variables or a `.env` file. Use `env.sample` as a reference configuration.

```
PEAKD_PLUGIN_PORT=3000
PEAKD_PLUGIN_DB_HOST=localhost
PEAKD_PLUGIN_DB_PORT=5432
PEAKD_PLUGIN_DB_NAME=haf
PEAKD_PLUGIN_DB_USER=username_readonly
PEAKD_PLUGIN_DB_PWD=super_secret_password
```

It is recommended to set the `search_path` to the database schema used for Hivemind:

```
ALTER ROLE username_readonly IN DATABASE haf SET search_path TO hivemind_schema;
```

Alternatively a specific database schema can be defined on the plugin:

```
PEAKD_PLUGIN_DB_SCHEMA=hivemind_schema
```

# Docker

Configuration of `.env` file is required to build the image. Please adjust values according to your configuration:

Build:

```
docker build . -t registry.gitlab.com/peakd/peakd-hivemind-plugin
```

Run:

```
docker run --name peakdhivemindplugin --rm -itd -p 127.0.0.1:3000:3000 registry.gitlab.com/peakd/peakd-hivemind-plugin
```
