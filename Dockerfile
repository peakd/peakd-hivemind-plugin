# Initiate a container to build the application in.
FROM node:18
WORKDIR /usr/src/app

# Configure '.env' file and copy the necessary files into container.
COPY .env ./
COPY package*.json ./

# Install app dependencies
RUN npm install

# Bundle app source
COPY . .

# Creates a "dist" folder with the production build
RUN npm run build

# Expose the web server's port.
# EXPOSE 3000

# Start the server using the production build
CMD [ "node", "dist/main.js" ]
